
<?php 
include "conexao.php";
include "cadastro.html";

$nome = $_POST['nome'];
$cpf = $_POST['cpf'];
$nascimento = $_POST['data'];
$matricula = $_POST['matricula'];
$semestre = $_POST['semestre'];


if (empty(trim($nome)) || !preg_match('/^[a-zA-Z ]+$/', $nome)) 
{
    echo "Nome inválido. O nome não pode conter caracteres especiais ou números.";
    exit;
}

if (empty(trim($cpf)) || !preg_match('/^\d{11}$/', $cpf)) 
{
    echo "CPF inválido.";
    exit;
}
try {
    $conn = new PDO("mysql:host=$server;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = "INSERT INTO `cadastro`(`cpf_aluno`, `Nome`, `Data_nasciment`, `num_matricula`, `semestre`) VALUES (?, ?, ?, ?, ?)";
    $stmt = $conn->prepare($sql);
    $stmt->execute([$cpf, $nome, $nascimento, $matricula, $semestre]);
    
    if($stmt->rowCount() > 0){
        // Cadastro realizado com sucesso, redirecionar para a página de login
        header("Location: cadastro.html");
        echo "Cadastro realizado com sucesso!";
       
        exit; // Certifique-se de usar o exit após o redirecionamento para interromper a execução do script atual
    } else {
        header("Location: cadastro.html");
        echo "Erro ao cadastrar!";
    }
    
    
    $conn = null; 
} catch (PDOException $e) {
    echo "Erro na execução da consulta: " . $e->getMessage();
}
?>
