<?php 

$server = "localhost";
$username = "root";
$password = "";
$dbname = "extensão";

try {
    $conn = new PDO("mysql:host=$server;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Conexão bem-sucedida";
} catch (PDOException $e) {
    echo "Falha na conexão: " . $e->getMessage();
}

?>



